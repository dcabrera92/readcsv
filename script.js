$(document).ready(function(e) {
    $("#upload").on('submit', (function(e) {
        e.preventDefault();
        $(".carousel-inner").empty();
        $('#loading').show();
        $.ajax({
            url: "ajax_php_file.php",
            type: "POST",
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
            success: function(data) {
                let dat = JSON.parse(data);
                let count = JSON.parse(data).length;
                $('#loading').hide();
                if (dat[0] !== 'Format is not supported') {
                    for (var i = 0; i < count; i++) {
                        let firstElement = $('<div id="m_' + i + '" class="item active">\
                        <div class="col-md-offset-2 col-md-8">\
                            <h2 class="col-md-offset-4 col-md-4"> Calling '+ dat[i].number +'</h2>\
                            <div class="col-md-offset-1 col-md-10"><h4 id="mens"></h4></div>\
                            <button data-slide="next" class="btn right col-md-offset-8 col-md-4"  href="#carousel-example-generic"><h3>Next Number</h3></button>\
                        </div>\
                    </div>');
                        let secuntsElements = $('<div id="m_' + i + '" class="item">\
                            <div class="col-md-offset-2 col-md-8">\
                                <h2 class="col-md-offset-4 col-md-4"> Calling '+ dat[i].number +'</h2>\
                                <div class="col-md-offset-4 col-md-8"><h4 id="mens"></h4></div>\
                                <button data-slide="next" class="btn right col-md-offset-8 col-md-4"  href="#carousel-example-generic"><h3>Next Number</h3></button>\
                            </div>\
                            </div>');
                        if (i !== 0 && i < count) {
                            secuntsElements.appendTo('.carousel-inner');
                        } else {
                            firstElement.appendTo('.carousel-inner');
                        }
                    }
                    $('.carousel-inner #mens:last').text('No more numbers to call.');
                } else {
                    alert(dat[0]);
                }
            }
        });
    }));
    $(function() {
        $("#file").change(function() {
            $(".carousel-inner").empty();
            var file = this.files[0];
        });
    });
    $('.carousel').carousel({
        interval: 0,
    });
});
